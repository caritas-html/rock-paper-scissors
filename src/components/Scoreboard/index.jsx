import {useState, useEffect} from "react"

const Scoreboard = ({winner}) => {
    const [machinePoints, setMachinePoints] = useState(0)
    const [userPoints, setUserPoints] = useState(0)

    const machineAddPoints = () => {
        setMachinePoints(machinePoints + 1)
    }

    const userAddPoints = () => {
        setUserPoints(userPoints + 1)
    }

    useEffect(() => {
        if (winner === "You lose") {
            machineAddPoints()            
        } else if (winner === "You win") {
            userAddPoints()
        } else if (winner === "Draw") {
            return true
        }
    }, [winner])


    return (
        <div className="scoreboard">
                <h1>You: {userPoints}</h1>
                <h1>Machine: {machinePoints}</h1>      
        </div>
    )
}

export default Scoreboard