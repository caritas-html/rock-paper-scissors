import {useState, useEffect} from 'react'
import Scoreboard from '../Scoreboard'
import Situation from '../Situation'
import './style.css'


const Game = () => {
    const [rock] = useState(0)
    const [paper] = useState(1)
    const [scissor] = useState(2)
    const [machineChoose, setMachineChoose] = useState("")
    const [clickedButton, setClickedButton] = useState("")
    const [actualUserChoose, setActualUserChoose] = useState("")
    const [winner, setWinner] = useState("")

    const comparingRock = () => {
        setClickedButton(0)
        machineChooser()
    }

    const comparingPaper = () => {
        setClickedButton(1)
        machineChooser()
    }

    const comparingScissor = () => {
        setClickedButton(2)
        machineChooser()
    }


    const machineChooser = () => {
        setMachineChoose(Math.round(Math.random () * 2))
        return machineChoose
        
    }

    // useEffect para saber qual é o botão clicado
    useEffect(() => {
        if (clickedButton === 0) {
            setActualUserChoose("Rock")
        } else if (clickedButton === 1) {
            setActualUserChoose("Paper")
        } else if (clickedButton === 2) {
            setActualUserChoose("Scissor")
        }
    }, [clickedButton])

    useEffect(() => {
        if (clickedButton === 0 && machineChoose === 0) {
            setWinner("Draw")
        } else if (clickedButton === 0 && machineChoose === 1) {
            setWinner("You lose")
        } else if (clickedButton === 0 && machineChoose === 2) {
            setWinner("You win")
        } else if (clickedButton === 1 && machineChoose === 1) {
            setWinner("Draw")
        } else if (clickedButton === 1 && machineChoose === 0) {
            setWinner("You win")
        } else if (clickedButton === 1 && machineChoose === 2) {
            setWinner("You lose")
        } else if (clickedButton === 2 && machineChoose === 2) {
            setWinner("Draw")
        } else if (clickedButton === 2 && machineChoose === 1) {
            setWinner("You win")
        } else if (clickedButton === 2 && machineChoose === 0) {
            setWinner("You lose")
        } else {
            setWinner("Not played yet")
        }
    }, [winner, clickedButton, machineChoose])

    return (
        <div className="board-game">
            <Scoreboard winner={winner}></Scoreboard>
      
            <Situation machineChoose={machineChoose} actualUserChoose={actualUserChoose} winner={winner}></Situation>
        <div className="buttons-div">
            <button onClick={comparingRock}>Rock</button>
            <button onClick={comparingPaper}>Paper</button>
            <button onClick={comparingScissor}>Scissor</button>
        </div>
        </div>
    )
}

export default Game