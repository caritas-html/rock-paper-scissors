import {useState, useEffect} from 'react'
import './style.css'

const Situation = ({machineChoose, actualUserChoose, winner}) => {

    const [actualChoose, setAcutalChoose] = useState("")


    useEffect(() => {
        if (machineChoose === 0) {
            setAcutalChoose("Rock")
        } else if (machineChoose === 1) {
            setAcutalChoose("Paper")
        } else if (machineChoose === 2) {
            setAcutalChoose("Scissor")
        } else {
            setAcutalChoose("C'mon, choose one")
        }
    }, [machineChoose])

    

    return (
        <>
        <div className="winner">
            <h2>{winner}</h2>
        </div>
        <div>
            <h3>machine: {actualChoose}</h3>
        </div>
        <div>
            <h3>you: {actualUserChoose}</h3>
        </div>
        </>
    )
}

export default Situation