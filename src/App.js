import './App.css';
import Game from './components/Game';
import Chooser from './components/Chooser';

function App() {
  return (
    <div className="game">
      <Chooser></Chooser>
    </div>
  );
}

export default App;
